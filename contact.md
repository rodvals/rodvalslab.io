---
layout: default
title: Contact
permalink: /contact/
katex: True
---
<h1 class="page-heading">Contact</h1>
If you want to get in touch with me or contact me for ideas or tips you can contact me at [andrea.valsecchi@studenti.unipd.it](mailto:andrea.valsecchi@studenti.unipd.it)
