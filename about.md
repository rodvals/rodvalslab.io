---
layout: default
title: About
permalink: /about/
katex: True
---

<h1 class="page-heading">About</h1>
My name is Andrea Valsecchi, I was born and raised in a little town in Italy called [Rovigo](https://www.youtube.com/watch?v=CSgx2gmRuwc).
I am currently studying Physics at Unipd in [Padova](https://www.youtube.com/watch?v=uQspCBa__c0).
{% include icon-varch.svg %} 
GNU/Linux lover.

#### Some project I made (WIP): 
<span class="icon">{% include icon-gitlab.svg %}</span> [multiple_pendulums](https://gitlab.com/rodvals/multiple-pendulums)

