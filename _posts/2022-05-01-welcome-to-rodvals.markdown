---
layout: post
title:  "Welcome to my Site!"
date:   2022-05-01 15:32:14 -0300
categories: website update
---

Here you will find things about me, some tutorials, some cool animations and if you aren't scared to inform yoursel about factual truths about animals check out more here: [Why](/vegan/) . </br>

If you want to ask me anything feel free to contacts me at: </br>
[andrea.valsecchi@studenti.unipd.it](andrea.valsecchi@studenti.unipd.it) 
